angular.module("zapApp")

	.directive('imageList', function() {return {
		restrict: 'E'
		,scope: {
			collection: '=',
			postId: '=',
			addImage: '&',
			rmImage: '&'
		}
		,controller: function($scope) {
			$scope.dragging = {
				inProgress: false
				,index: 0
				,aboveIndex: 0
				,mouseAbove: true
				,dragging: function(index) {
					return this.inProgress && this.index == index;
				}
				,above: function(index) {
					return this.inProgress && this.index != index &&
							this.aboveIndex == index && this.mouseAbove;
				}
				,below: function(index) {
					return this.inProgress && this.index != index &&
							this.aboveIndex == index && !this.mouseAbove;
				}
				,start: function(index) {
					this.inProgress = true;
					this.index = index;
					return false;
				}
				,dragOver: function(index, event) {
					if (this.inProgress) {
						var y = event.pageY - $(event.target).offset().top;
						this.mouseAbove = y < $(event.target).height() / 2;
						this.aboveIndex = index;
						event.preventDefault();
						return false;
					}
				}
				,end: function() {
					if (this.inProgress && this.index != this.aboveIndex) {
						if (this.mouseAbove) {
							$scope.insertBefore(this.index, this.aboveIndex);
						} else {
							$scope.insertAfter(this.index, this.aboveIndex);
						}
					}
					this.inProgress = false;
				}
			};

			$scope.insertAfter = function(i, j) {
				var move = $scope.collection[i];
				$scope.collection.splice(i, 1);
				if (i < j) {
					$scope.collection.splice(j, 0, move);
				} else if (i > j) {
					$scope.collection.splice(j + 1, 0, move);
				}

			};
			$scope.insertBefore = function(i, j) {
				var move = $scope.collection[i];
				$scope.collection.splice(i, 1);
				if (i < j) {
					$scope.collection.splice(j - 1, 0, move);
				} else if (i > j) {
					$scope.collection.splice(j, 0, move);
				}

			};

			$scope.handleFiles = function(e) {
				e.stopPropagation();
				e.preventDefault();

				var files = e.dataTransfer.files;
				angular.forEach(files, function(file) {
					if (!file.type.match('image.*')) {
						return;
					}
					$scope.addImage({'file': file});
				});
				return false;
			};

			$scope.del = function(index) {
				$scope.rmImage({'index': index});
			};
		}
		,link: function($scope, element, attrs) {
			var dropArea = element.find('.image-drop');
			dropArea.on('drop', function(e) {return $scope.$apply(function(){
				e.dataTransfer = e.originalEvent.dataTransfer;
				return $scope.handleFiles(e);
			})});
			dropArea.on('dragover', function(e) {$scope.$apply(function(){
				e.stopPropagation(); 
				e.preventDefault();
				e.originalEvent.dataTransfer.dropEffect = 'copy';
				return false;
			})});
		}
		,templateUrl: '/static/partials/image-list.html'
	};})

	.directive('cancelSelection', function(){return {
		link: function(scope, element, attrs) {
			element.on('selectstart', function(){return false});
			element.css({
				'-moz-user-select': 'none'
				,'-webkit-user-select': 'none'
				,'user-select': 'none'
			});
		}
	};})
;
