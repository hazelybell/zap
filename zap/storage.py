import flask, json, os, shutil, uuid

import scss as scss_lib

from datetime import datetime
from PIL import Image

from .models import ImageAttachment, Post


class SinglePostStorage(object):
	def __init__(self, posts_storage):
		self.storage = posts_storage

	def set_post(self, post):
		self.post = post

	def update_images(self, new_images):
		img_attachments = [self.load_image(new_image) for new_image in new_images]
		self.storage.clean_images(self.post, img_attachments)
		return img_attachments

	def load_image(self, img_data):
		img = self.storage.load_image(self.post, img_data['name'])
		img.caption = img_data['caption']
		return img


class PostsStorage(object):
	def __init__(self, folder):
		self.folder = folder

	def find_all(self):
		for dirpath, dirnames, filenames in os.walk(self.folder):
			if 'thing.json' in filenames:
				# this folder is a thing!
				content = open(os.path.join(dirpath, 'body.md')).read()
				data = json.load(open(os.path.join(dirpath, 'thing.json')))
				data['content'] = content
				post = Post(data['id'], SinglePostStorage(self))
				post.update(data)
				yield post
				

	def save(self, post):
		path = self.get_folder_for_post(post)
		os.makedirs(path, exist_ok=True)

		open(self.os_path(post, 'body.md'), 'w').write(post.content or '')
		post_data = {
			'id': str(post.id)
			,'title': post.title
			,'timestamp': post.timestamp
			,'fave': post.fave
			,'labels': post.labels
			,'slug': post.slug
			,'images': [img.for_json() for img in post.images]
		}
		open(self.os_path(post, 'thing.json'), 'w').write(json.dumps(post_data))

	def delete(self, post):
		shutil.rmtree(self.get_folder_for_post(post))
	
	def get_folder_for_post(self, post):
		timestamp = datetime.fromtimestamp(post.timestamp)
		path = [self.folder, timestamp.strftime('%Y-%m-%d'), post.id]
		return os.path.join(*map(str, path))

	def os_path(self, post, f):
		return os.path.join(self.get_folder_for_post(post), f)

	def upload_path(self, post, f):
		return self.os_path(post, f)

	# image stuff

	def image_path(self, post, img):
		return self.upload_path(post, img.name)

	def thumbnail_path(self, post, img, size):
		size = img.normalize_size(size)
		return self.upload_path(post, img.get_thumbnail_name(size))

	def load_image(self, post, img_id):
		path = self.upload_path(post, img_id)
		image = Image.open(path)
		return ImageAttachment(img_id, image.size)
		
	def save_image(self, post, img_id, img_data):
		path = self.upload_path(post, img_id)
		# TODO: resolve already-in-use paths
		img_data.save(path)
		image = Image.open(path)
		return post.add_image(ImageAttachment(img_id, image.size))

	def make_thumbnail(self, post, img, size):
		size = img.normalize_size(size)
		path = self.thumbnail_path(post, img, size)
		if not os.path.exists(path):
			image = Image.open(self.image_path(post, img))
			image.thumbnail(size)
			image.save(path)
		return path

	def delete_image(self, post, image_id):
		os.remove(self.upload_path(post, image_id))

	def clean_images(self, post, new_images, inject=None):
		"""action is dependency injectable, taken on each unwanted image"""
		action = inject or self.delete_image

		folder = self.get_folder_for_post(post)
		image_names = set(img.name for img in new_images)
		for dirpath, dirnames, filenames in os.walk(folder):
			for f in filenames:
				file_extension = os.path.splitext(f)[1] 
				if f in image_names or file_extension in ['.json', '.md']:
					continue
				action(post, f)
			break


class SCSS(object):
	def __init__(self, site):
		self.site = site
		scss_lib.config.STATIC_ROOT = os.path.join(site.folder, 'html')
		scss_lib.config.LOAD_PATHS = [os.path.join(site.folder, 'html')]
		self.scss = scss_lib.Scss(scss_opts={
			'compress': True
		})

	def compile(self, fname):
		fname = os.path.join(self.site.folder, 'html', fname)
		try:
			return self.scss.compile(open(fname).read())
		except FileNotFoundError as err:
			flask.abort(404)
		except scss_lib.errors.SassEvaluationError as err:
			return err.to_css()
