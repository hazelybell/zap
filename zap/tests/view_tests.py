from ..models import Post
from ..views import PostSorter

import datetime, random, unittest



class TestPostSorting(unittest.TestCase):
	def setUp(self):
		self.posts = []
		for i in range(0, 20):
			self.posts.append(Post().update({
				'timestamp': datetime.datetime(year=2012, month=1, day=i + 1)
				, 'title': 'post({})'.format(i)
			}))

	def test_sort_by_date(self):
		sorter = PostSorter()
		sorted_posts = sorter.sort(self.posts)
		self.assertEqual(sorted_posts, list(reversed(self.posts)),
			"failed to sort list with no stars by date, newest first")

		self.posts[0].update({
			'timestamp': datetime.datetime(year=2013, month=1, day=1)
		})
		sorted_posts = sorter.sort(self.posts)
		self.assertEqual(sorted_posts[0], self.posts[0])
		self.assertEqual(sorted_posts[1:], list(reversed(self.posts[1:])))


	def test_sort_with_stars(self):
		sorter = PostSorter()
		self.posts[5].update({
			'fave': True
		})
		self.posts[0].update({
			'fave': True
		})

		sorted_posts = sorter.sort(self.posts)

		print([str(x) for x in sorted_posts])
		# faved posts should be sorted by date at the top
		self.assertEqual(sorted_posts[0], self.posts[5])
		self.assertEqual(sorted_posts[1], self.posts[0])

		self.posts.append(self.posts[0])
		self.posts.append(self.posts[5])
		del self.posts[5]
		del self.posts[0]

		self.assertEqual(sorted_posts, list(reversed(self.posts)))
