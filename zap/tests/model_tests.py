from ..models import ImageAttachment, Post, PostCollection
from ..storage import SinglePostStorage

import datetime, json, random, unittest, unittest.mock, uuid

from .common import PostFactory


class TestPostModel(unittest.TestCase):
	def setUp(self):
		self.post = Post()

	def test_to_json_nones(self):
		result = json.loads(self.post.to_json())
		self.assertEqual(result['content'], '',
			'empty content should remain empty after json roundtrip')

	def test_to_json_fave(self):
		result = json.loads(self.post.to_json())
		self.assertEqual(result['fave'], False)
		self.post.update({'fave': True})
		result = json.loads(self.post.to_json())
		self.assertEqual(result['fave'], True)

	def test_to_json_labels(self):
		"""to_json() preserves labels"""
		self.post.update({'labels': ['abc', 'b', 'c']})
		result = json.loads(self.post.to_json())
		self.assertEqual(set(result['labels']), {'abc', 'b', 'c'})

	def test_makes_ID(self):
		self.assertIsNotNone(self.post.id)
		self.assertIsInstance(self.post.id, uuid.UUID)
		self.assertNotEqual(self.post.id, Post().id,
			"id should be unique per-post")

	def test_parses_ID(self):
		self.post = Post(id=str(uuid.uuid4()))
		self.assertIsInstance(self.post.id, uuid.UUID)
		
	def test_calls_update_images(self):
		self.post.storage = unittest.mock.Mock(SinglePostStorage)
		imgs = [ImageAttachment('test.png', (50, 100))]
		imgs_json = [i.to_json() for i in imgs]
		self.post.storage.update_images.return_value = imgs
		self.post.update({'images': imgs_json})

		self.post.storage.update_images.assert_called_once_with(imgs_json)

	def test_calls_listeners_on_update(self):
		listeners = [unittest.mock.Mock(), unittest.mock.Mock()]
		for listener in listeners:
			self.post.add_listener(listener)

		self.post.update({'title': 'new title'})
		for listener in listeners:
			listener.post_updated.assert_called_once_with(self.post)


class TestPostIdentifier(unittest.TestCase):
	def setUp(self):
		self.post = Post().update({'slug': 'cool-shiz'})
		self.other = Post().update({'slug': 'uncool-shiz'})

	def test_slug_identifier(self):
		"""can identify posts by slug"""
		matcher = Post.match_slug('cool-shiz')
		self.assertTrue(matcher.matches(self.post))
		self.assertFalse(matcher.matches(self.other))

	def test_id_identifier(self):
		"""can identify posts by id"""
		matcher = Post.match_id(self.post.id)
		self.assertTrue(matcher.matches(self.post))
		self.assertFalse(matcher.matches(self.other))


class TestPostCollection(unittest.TestCase):
	def setUp(self):
		self.postFactory = PostFactory(timestamp=PostFactory.SEQ,
			title=PostFactory.SEQ,
			fave=False,
			labels=['all', PostFactory.even_odd,
						lambda i: 'low' if i < 10 else None])
		self.posts = self.postFactory.make_posts(20)
		self.collection = PostCollection(self.posts)

	def test_add_post(self):
		"""can add new posts"""
		self.collection = PostCollection([])
		self.assertEqual(len(self.collection.all()), 0)

		post = Post()
		self.collection.add(post)
		self.assertEqual(self.collection.all()[0], post)


	def test_delete_post(self):
		"""can delete posts"""
		random.shuffle(self.posts)

		total = len(self.collection.all())
		for post in self.posts:
			self.collection.delete(post)
			new_total = len(self.collection.all())
			self.assertEqual(new_total, total - 1)
			total = new_total

			self.assertNotIn(post, self.collection.all())


	def test_can_find_by_slug(self):
		self.assertIsNone(self.collection.find_one(
							Post.match_slug("fake_slug")))

		for post in self.posts:
			result = self.collection.find_one(
						Post.match_slug(post.slug))
			self.assertIsNotNone(result, "should find a post")
			self.assertEqual(result.id, post.id, "should find the correct post")

	def test_can_find_by_id(self):
		unused_id = Post().id
		self.assertIsNone(self.collection.find_one(Post.match_id(id)))

		for post in self.posts:
			result = self.collection.find_one(Post.match_id(post.id))
			self.assertIsNotNone(result, "should find a post")
			self.assertEqual(result.id, post.id, "should find the correct post")

	def test_can_find_by_multiple_ids(self):
		posts = [self.posts[i] for i in range(0, 20, 2)]
		result = self.collection.find_by_id([p.id for p in posts])
		self.assertEqual(posts, result)

	def test_can_find_by_labels(self):
		evens = [self.posts[i] for i in range(0, 20, 2)]
		odds = [self.posts[i] for i in range(1, 20, 2)]
		lows = [self.posts[i] for i in range(0, 10)]

		self.assertEqual(self.collection.find(['all']), self.posts)
		self.assertEqual(self.collection.find(['even']), evens)
		self.assertEqual(self.collection.find(['odd']), odds)
		self.assertEqual(self.collection.find(['low']), lows)
		self.assertEqual(self.collection.find(['even', 'low']), evens[:5])
		self.assertEqual(self.collection.find(['odd', 'low']), odds[:5])

	def test_calls_listeners_on_add(self):
		listeners = [unittest.mock.Mock(), unittest.mock.Mock()]
		for listener in listeners:
			self.collection.add_listener(listener)

		post = Post()
		self.collection.add(post)
		for listener in listeners:
			listener.post_added.assert_called_once_with(post)

	def test_calls_listeners_on_delete(self):
		post = Post()
		self.collection.add(post)

		listeners = [unittest.mock.Mock(), unittest.mock.Mock()]
		for listener in listeners:
			self.collection.add_listener(listener)

		self.collection.delete(post)
		for listener in listeners:
			listener.post_deleted.assert_called_once_with(post)
